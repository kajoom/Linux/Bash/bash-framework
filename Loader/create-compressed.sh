#!/bin/bash
# Copy/Rename ./load-framework.minified.sh to load-framework.compressed.sh
# with variables names replaced.

# Script and Command options.
SELF_PATH=$(realpath $0)
SELF_LOC=$(dirname "${SELF_PATH}")

_getINITIAL() {
	echo "${1}" | head -n 1 | cut -d= -f1 | sed 's/=//g'
}

_getREPLACEMENT() {
	echo "${1}" | head -n 1 | cut -d= -f2 | sed 's/=//g'
}

_renameFILE() {
	
	local REPLACE=()
	local FILE_SRC="${SELF_LOC}/load-framework.minified.sh"
	local FILE_DST="${SELF_LOC}/load-framework.compressed.sh"
	
	# Replacements.
	# REPLACE+=("INITIAL;REPLACEMENT" )
	
	# Temporarily save.
	REPLACE+=("FRAMEWORK_SCRIPT_FILENAME=_F1S1F1_" )
	
	# Standard replacements.
	REPLACE+=("PLATFORM_BRANCH=PFB" )
	REPLACE+=("VERSION_BRANCH=VB" )
	REPLACE+=("IS_FILE_FOUND=IFF" )
	REPLACE+=("FILE_PATH_FOUND=FPF" )
	
	REPLACE+=("VERSION_VARNAME=VS_VAR" )
	REPLACE+=("FILENAME_VARNAME=FN_VAR" )
	REPLACE+=("LOADED_VARNAME=LD_VAR" )
	
	REPLACE+=("FILENAME=FN" )
	REPLACE+=("SEED=SD" )
	
	REPLACE+=("_getVERSIONBRANCH=_getVB" )
	REPLACE+=("LOADER_VERSION=LV" )
	REPLACE+=("ALIAS=AL" )
	REPLACE+=("PLATFORM=PL" )
	REPLACE+=("PREFIX=PF" )
	REPLACE+=("RELATIVE_PATH=RP" )
	REPLACE+=("NAME=NM" )
	REPLACE+=("LOAD_PATHS=LP" )
	
	REPLACE+=("FILE_PATH=FP" )
	REPLACE+=("LOCATION=LC" )
	
	# Put back.
	REPLACE+=("_F1S1F1_=FRAMEWORK_SCRIPT_FILENAME" )

	
  if [ -f "${FILE_SRC}" ] ; then 
  
    \cp -av "${FILE_SRC}" "${FILE_DST}"
    
	  for rep in "${REPLACE[@]}" ; do
	  
	    local INITIAL=$(_getINITIAL "${rep}")
	    local REPLACEMENT=$(_getREPLACEMENT "${rep}")
	    
	    sed -i "s/${INITIAL}/${REPLACEMENT}/g" "${FILE_DST}"
	    
	    echo " Done '${INITIAL}' to '${REPLACEMENT}' replacement!"
	    
	  done;
    
  else
    echo " Source file do not exits!"
    exit 1;
  fi
}

echo;

_renameFILE;

exit 0;
