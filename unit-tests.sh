#!/bin/bash
# Unit Tests Script.
# Version: 0.2.2
# Authors : Marc-Antoine Minville - Kajoom.Ca
# 2021-01-12 (v0.2.2) - Improved framework and added tools.
# 2021-01-06 (v0.2.1) - New bash version.
# Download as .sh file and run it. eg: 
# wget -O unit-tests.sh https://cloud.kajoom.net/s/unittestssh/download
# bash ./unit-tests.sh
# bash ./unit-tests.sh --help

########################################################################
# Kajoom Framework for Bash - The Kajoom Framework for Bash.
# Copyright (C) 2021  Marc-Antoine Minville - KAJOOM
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
########################################################################

# KEYWORD Kajoom Script Version. Be sure to keep the same as in header comments.
SCRIPT_VERSION='0.2.2'
SCRIPT_NAME='Unit Tests Script'
SCRIPT_DESCRIPTION='Unit Tests for KAJOOM Framework Bash Scripts'
SCRIPT_URL='https://cloud.kajoom.net/s/unittestssh'
SCRIPT_DOWNLOAD_URL='https://cloud.kajoom.net/s/unittestssh/download'
SCRIPT_ALIAS='unit-tests'
SCRIPT_FILENAME='unit-tests.sh'
SCRIPT_START=$(date +%s)
SCRIPT_ENV="prod" # [prod|stage|dev|alpha|beta|test]

# Script and Command options.
SELF_PATH=$(realpath $0)
SELF_LOC=$(dirname "${SELF_PATH}")
INIT_LOC=$(pwd)
CURRENT_PID=$$
VERBOSITY=""

# Framework Loader.
[[ "$1" =~ ^-[v]*$ ]] && VERBOSITY="${1}"; _loadFRAMEWORK(){ local LV="0.2.2" AL="${1}" PL="${2}" PF="${3}" RP="${4}" NM="KAJOOM Framework" FN="${1}.sh" SD LP=() VS_VAR FN_VAR LD_VAR FP LC IFF=no FPF="" FPA; SD=$(echo "$FN" | sed -e 's/[-_\.]//g'); [ -n "${PF}" ] && [ "kajoom" != "${PF}" ] && FN="${AL}-${PF}.sh" || PF="KAJOOM" && NM="${PF^^} Framework"; PF_="${PF^^}_"; VS_VAR="${PF_}FRAMEWORK_SCRIPT_VERSION"; FN_VAR="${PF_}FRAMEWORK_SCRIPT_FILENAME"; LD_VAR="${PF_}FRAMEWORK_LOADED"; declare -g $FN_VAR="${FN}"; declare -g $LD_VAR=no; [ -n "${RP}" ] && LP+=("${RP}" "${SELF_LOC}/${RP}") ; LP+=("" "./" "${SELF_LOC}/" "${INIT_LOC}/" "${HOME}/" "${HOME}/scripts/" "${HOME}/bin/"); for LC in ${LP[*]} ; do FP="${LC}${!FN_VAR}"; FPA="${LC}Kajoom-Framework/${!FILENAME_VARNAME}"; if [ -f "${FP}" ]; then IFF=yes ; FPF="${FP}"; elif [ -f "${FPA}" ]; then IFF=yes ; FPF="${FPA}"; fi; if [ "yes" == "${IFF}" ]; then [ "-vvv" == "${VERBOSITY}" ] && echo -e "\n ${NM} found here: ${FPF}" ; source "${FPF}" && break; fi; done; if [ "yes" != "${!LD_VAR}" ]; then echo -e "\n Cannot find any ${NM^^} Script in PATH or common places! "; echo " Please download the ${!FN_VAR} script with the following: "; echo -e " wget -O ${!FN_VAR} https://cloud.kajoom.net/s/${SD}/download \n"; exit 1; fi; if [ -n "${PL}" ] && [ "${PL}" != "${!VS_VAR}" ]; then [ "-vvv" == "${VERBOSITY}" ] && echo -e "\n Required platform version not fullfilled: ${PL} != ${!VS_VAR} \n"; exit 1; fi;}

# Load the Kajoom Framework.
_loadFRAMEWORK "kajoom-framework" "0.2.2"

# Load the KAJOOM Customizations.
#_loadFRAMEWORK "kajoom-framework" "0.2.2" "overrides" "Kajoom-Framework/"

# Kajoom AMK Framework.
#_loadFRAMEWORK "kajoom-framework" "0.2.2" "amk" "Anti-Malware-Kit/"

# Load global configs.
[ -f "${CONFIG_FILE}" ] && source "${CONFIG_FILE}"


# Tests.
_testFOLDER() {
	if [ "--warn" == "${2}" ] ; then
    [ -d "${1}" ] && _addSUCCESS || _addWARNING
  else
    [ -d "${1}" ] && _addSUCCESS || _addERROR
  fi
}

_testCONDITION() {
	
	if [ "--warn" == "${3}" ] ; then
    [[ `${1}` == ${2} ]] && _addSUCCESS || _addWARNING
  else
    [[ `${1}` == ${2} ]] && _addSUCCESS || _addERROR
  fi
}

_testNUMBERS() {
	
	if [ "--warn" == "${3}" ] ; then
    [[ ${1} -eq ${2} ]] && _addSUCCESS || _addWARNING
  else
    [[ ${1} -eq ${2} ]] && _addSUCCESS || _addERROR
  fi
}

_testRETURN() {
	if [ "--false" == "${2}" ] ; then
	  if [ "--warn" == "${3}" ] ; then
	    `${1} >/dev/null` && _addWARNING || _addSUCCESS
	  else
	    `${1} >/dev/null` && _addERROR || _addSUCCESS
	  fi
	else
	  if [ "--warn" == "${2}" ] ; then
	    `${1} >/dev/null` && _addSUCCESS || _addWARNING
	  else
	    `${1} >/dev/null` && _addSUCCESS || _addERROR
	  fi
	fi
}

# Unit tests start.
_unitTESTS() {
	
	echo;
	colorText green ' Going to perform defined Unit Tests!' ; sleep 0.2 ;
	echo;
	
	# Local variables.
	local CHECK=''
	local RESULT=''
	local ERRORS=0
	local WARNINGS=0
	local SUCCESS=0
	
	# Define checks.
	local UNIT_TESTS=(config_folder)
	
	### Basic checks ###
	
	# Config Folder.
  _addCHECK "config_folder" "Config Folder should exist.";
  _testFOLDER "${CONFIG_FOLDER}"
	
	# Log Folder.
  _addCHECK "log_folder" "Log Folder should exist.";
  _testFOLDER "${LOG_FOLDER}"
	
	# Log Folder Admin.
  _addCHECK "log_folder_admin" "Log Folder Admin should exist.";
  _testFOLDER "${LOG_FOLDER_ADMIN}"
	
	# Reports Folder.
  _addCHECK "reports_folder" "Reports Folder should exist.";
  _testFOLDER "${REPORTS_FOLDER}" --warn
  
	# Test functions.
	
	# _userEXISTS
	_addCHECK "function:userEXISTS:root" "root user should exist.";
  _testCONDITION "_userEXISTS root" yes
  
  _addCHECK "function:userEXISTS:nobody" "nobody user should exist.";
  _testCONDITION "_userEXISTS nobody" yes
  
  _addCHECK "function:userEXISTS:inexistent111" "inexistent111 user should not exist.";
  _testCONDITION "_userEXISTS inexistent111" no
  
  # _isMD5
	_addCHECK "function:_isMD5:valid" "68b329da9893e34099c7d8ad5cb9c940 should be a valid MD5.";
  _testRETURN "_isMD5 68b329da9893e34099c7d8ad5cb9c940"
  
	_addCHECK "function:_isMD5:invalid" "helloworld123456 should NOT be a valid MD5.";
  _testRETURN "_isMD5 helloworld123456" --false  --warn
  
  # Resources.
  # Register resources.
  _addCHECK "register_resource" "Resources should be registerable.";
	_addRESOURCE "log" "${LOG_FILE}"
	#_addRESOURCE "log" "${LOG_FILE}" # Should fail because twice.
	_addRESOURCE "admin" "${LOG_FILE_ADMIN}"
	_addRESOURCE "report" "${REPORT_FILE}"
	_testNUMBERS `echo "${RESOURCES_LIST[@]}" | sed 's/ /\n/g' | wc -l` 3
	
	# Get resource.
	_addCHECK "get_resource:log" "Should be able to get a registered resource.";
	_testRETURN "_getRESOURCE log"
	
	_addCHECK "get_resource:bob" "Should NOT be able to get an un-registered resource.";
	_testRETURN "_getRESOURCE inexistent111" --false
	
	# Get all resources.
	_addCHECK "get_resources" "Should be able to get the list of all registered resources.";
	# Get all resources list (return format: id=path).
	_testNUMBERS `_getRESOURCES | wc -l` 3
	
	# Create resources.
	_addCHECK "create_resources" "Should be able to create resources.";
	# Create all resources files.
	_testRETURN "createRESOURCES"
	
	_addCHECK "create_resource:single" "Should be able to create a single registered resource.";
	# Create a resource file.
	_testRETURN "_addRESOURCE single && createRESOURCE single"
	
	#_addCHECK "create_resource:inexistent111" "Should NOT be able to create a single un-registered resource.";
	# Create a resource file.
	#_testRETURN "createRESOURCE inexistent111" --false
	#createRESOURCE inexistent111
  
  ### Data Errors checks ###
	# Softwares hashes lines should not be empty.
	#CHECK=hashes_line_empty
  #_addCHECK;
  #CHECK_RESULT=$(echo "${SOFTWARES_HASHES}" | grep -v .)
  #CHECK_COUNT=$(echo "${CHECK_RESULT}" | wc -l)
  #if [ ${CHECK_COUNT} -gt 1 ] ; then
  #  _addERROR;
  #  echo "  Empty lines: ${CHECK_COUNT}"
  #  echo "  Empty content: ${CHECK_RESULT}"
  #  echo;
  #else
  #  _addSUCCESS;
  #fi
  
  ### Integrity checks ###
  
	
	# Report.
	_printREPORT;
	
}


# Display Report.
_printREPORT() {
	
	[ ${ERRORS} -gt 0 ] && MY_COLOR=red || MY_COLOR=green
	[ ${ERRORS} -gt 0 ] && MY_ACTION='failed' || MY_ACTION='completed'
  LOG_MSG="Unit tests ${MY_ACTION}"
  colorText ${MY_COLOR} " ${LOG_MSG}!" ; sleep 0.05 ;
  _writeLOG "${LOG_MSG}."
  echo;
  
  echo " Success: ${SUCCESS}"
  echo " Warnings: ${WARNINGS}"
  echo " Errors: ${ERRORS}"
	echo;
}


# Call the menu controller.
_menuCONTROLLER "$@"

echo;

_showHEADER;
_getUSERINFOS;

if [ "$PROCEED" == y ] ; then
  _unitTESTS;
fi

_showFOOTER;
# Clean.
_cleanTEMP;
exit 0;
