#!/bin/bash
# Starter Script Example.
# Version: 0.2.2
# Authors : Marc-Antoine Minville - Kajoom.Ca
# 2021-01-12 (v0.2.2) - Added framework overrides option.
# 2021-01-05 (v0.2.1) - Now working with the AMK Framework as a dependency.
# 2021-01-03 (v0.2.0) - Now working with the Kajoom Framework as a dependency.
# 2020-12-21 (v0.0.1) - New bash version.
# Download as .sh file and run it. eg: 
# wget -O starter-script.example.sh https://cloud.kajoom.net/s/starterscriptexamplesh/download
# bash ./starter-script.example.sh
# bash ./starter-script.example.sh --help

########################################################################
# Kajoom Framework for Bash - The Kajoom Framework for Bash.
# Copyright (C) 2021  Marc-Antoine Minville - KAJOOM
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
########################################################################

# KEYWORD Kajoom Script Version. Be sure to keep the same as in header comments.
SCRIPT_VERSION='0.2.2'
SCRIPT_NAME='Starter Script Example'
SCRIPT_DESCRIPTION='KAJOOM Starter Script Example'
SCRIPT_URL='https://cloud.kajoom.net/s/starterscriptexamplesh'
SCRIPT_DOWNLOAD_URL='https://cloud.kajoom.net/s/starterscriptexamplesh/download'
SCRIPT_ALIAS='starter-script-example'
SCRIPT_FILENAME='starter-script.example.sh'
SCRIPT_START=$(date +%s)
SCRIPT_ENV="prod" # [prod|stage|dev|alpha|beta|test]
SCRIPT_AUTHOR="My Name" # Change to your own name.

# Script and Command options.
SELF_PATH=$(realpath $0)
SELF_LOC=$(dirname "${SELF_PATH}")
INIT_LOC=$(pwd)
CURRENT_PID=$$
VERBOSITY=""

# Framework Loader.
[[ "$1" =~ ^-[v]*$ ]] && VERBOSITY="${1}"; _getVB(){ echo "${1}" | cut -d '.' -f1,2;}; _loadFRAMEWORK(){ local LV="0.2.2" AL="${1}" PL="${2}" PF="${3}" RP="${4}" NM="KAJOOM Framework" FN="${1}.sh" LP=() VS_VAR FN_VAR LD_VAR FP LC PFB VB IFF FPF; local SD=$(echo "$FN" | sed -e 's/[-_\.]//g'); [ -n "${PF}" ] && [ "kajoom" != "${PF}" ] && FN="${AL}-${PF}.sh" || PF="KAJOOM" && NM="${PF^^} Framework"; PF_="${PF^^}_"; VS_VAR="${PF_}FRAMEWORK_SCRIPT_VERSION"; FN_VAR="${PF_}FRAMEWORK_SCRIPT_FILENAME"; LD_VAR="${PF_}FRAMEWORK_LOADED"; declare -g $FN_VAR="${FN}"; declare -g $LD_VAR=no; [ -n "${RP}" ] && LP+=("${RP}" "${SELF_LOC}/${RP}"); LP+=("" "./" "${SELF_LOC}/" "${INIT_LOC}/" "${HOME}/" "${HOME}/scripts/" "${HOME}/bin/"); IFF=no; FPF=""; for LC in ${LP[*]}; do FP="${LC}${!FN_VAR}"; FP_ALT="${LC}Kajoom-Framework/${!FN_VAR}"; if [ -f "${FP}" ] ; then IFF=yes ; FPF="${FP}"; elif [ -f "${FP_ALT}" ]; then IFF=yes; FPF="${FP_ALT}"; fi; if [ "yes" == "${IFF}" ] ; then [ "-vvv" == "${VERBOSITY}" ] && echo -e "\n ${NM} found here: ${FPF}" ; source "${FPF}" && break; fi; done; if [ "yes" != "${!LD_VAR}" ]; then echo -e "\n Cannot find any ${NM^^} Script in PATH or common places! "; echo " Please download the ${!FN_VAR} script with the following: "; echo -e " wget -O ${!FN_VAR} https://cloud.kajoom.net/s/${SD}/download \n"; exit 1; fi; PFB=$(_getVB "${PL}"); VB=$(_getVB "${!VS_VAR}"); if [ -n "${PFB}" ] && [ "${PFB}" != "${VB}" ]; then [ "-vvv" == "${VERBOSITY}" ] && echo -e "\n Required platform version not fullfilled: ${PFB} != ${VB} \n"; exit 1; fi;}

# Load the Kajoom Framework.
_loadFRAMEWORK "kajoom-framework" "0.2.2"

# Load the KAJOOM Customizations.
#_loadFRAMEWORK "kajoom-framework" "0.2.2" "overrides"

# Load global configs.
[ -f "${CONFIG_FILE}" ] && source "${CONFIG_FILE}"

_starterSCRIPT() {
	echo;
	colorText green ' Going to Start the Script!' ; sleep 0.2 ;
	echo;
	# First check.
	# Action.
	# Confirmation.
}

# Call the menu controller.
_menuCONTROLLER "$@"

echo;

_showHEADER;
_getUSERINFOS;

if [ "$PROCEED" == y ] ; then
  _starterSCRIPT;
fi

_showFOOTER;
# Clean.
_cleanTEMP;
exit 0;
