
	888                d8b
	888                Y8P
	888
	888  888  8888b.  8888  .d88b.   .d88b.  88888b.d88b.
	888 .88P     "88b "888 d88""88b d88""88b 888 "888 "88b
	888888K  .d888888  888 888  888 888  888 888  888  888
	888 "88b 888  888  888 Y88..88P Y88..88P 888  888  888
	888  888 "Y888888  888  "Y88P"   "Y88P"  888  888  888
	                  888
	  KAJOOM TOOLS   d88P          :: THE KAJOOM FRAMEWORK
	               888P        STARTER FOR BASH SCRIPTS ::

	======================================================
	|            The Kajoom Framework for Bash           |
	======================================================
	                 Version : 0.2.2


# Kajoom Framework for Bash

*The Kajoom Framework for Bash (v0.2.2)*

A set of common variables and functions to speed-up Bash script development, uniformity and quality.

Contains all the functions to display and process common tasks to a single file which acts now as a Bash library and framework system ;)

Feel free to fork, copy or contribute!


## Getting Started

Here is an example of how to deploy and use the Kajoom Framework for Bash in few seconds!


#### Download the framework

First, go to the location where you want the script to be installed 
(optionnaly create a new folder) and download the framework:

	mkdir -p ~/scripts
	cd ~/scripts
	wget -O kajoom-framework.sh https://cloud.kajoom.net/s/kajoomframeworksh/download


#### Download the script and run it!

Then, download any script compatible with the framework. We'll start with 
the Starter Script Example:

	wget -O starter-script.example.sh https://cloud.kajoom.net/s/starterscriptexamplesh/download
	bash ./starter-script.example.sh --help


#### Make it your own

Then, maybe rename it to a more useful and personal name like `my-script.sh` and run it:

	cp -av ./starter-script.example.sh ./my-script.sh
	bash ./my-script.sh


##### Optionnally make the file executable

You can make the file executable like this, so that next time instead of 
using the previous command to run the script, you will be able to use 
the shorter version:

	chmod +x ./my-script.sh
	my-script.sh


#### That's it!

You are ready to go! You can continue to edit the my-script.sh file until you are satisfied with your creation.

If you wish to benefit from the self-update functionality, upload your script to a public location and change the `SCRIPT_DOWNLOAD_URL` variable in the header part of the file.

---

## Downloads

#### Scripts

#### Required Dependencies

Mandatory dependencies to be able to run the scripts.

- [kajoom-framework.sh](https://cloud.kajoom.net/s/kajoomframeworksh/download) : *Kajoom Framework for Bash* (Kajoom Framework for Bash Scripts)

#### Utilities

Some useful utilities included in the pack.

- [starter-script.example.sh](https://cloud.kajoom.net/s/starterscriptexamplesh/download) : *Starter Script Example* (Starter Script Example)
- [kajoom-framework-overrides.sh](https://cloud.kajoom.net/s/kajoomframeworkoverridessh/download) : *Overrides Examples* (Overrides for the Kajoom Framework)
- [unit-tests.sh](https://cloud.kajoom.net/s/unittestssh/download) : *Unit Tests Script* (Unit Tests for Kajoom Framework Bash Scripts)
- [Loader/](https://cloud.kajoom.net/s/kajoomframeworkbashdev?path=%2FLoader) : *Loader Folder* (Different versions of the framework loader function)

---

## Going further


#### Documentation

- [Kajoom Framework for Bash](https://cloud.kajoom.net/s/kajoomframeworkbash) : *You are here!* (Main documentation for the Kajoom Framework for Bash)

---

## Changelog

- 2021-01-12 (v0.2.2) - Improved framework and added tools.
- 2021-01-05 (v0.2.1) - Development version.
- 2021-01-02 (v0.2.0) - New bash version.

---

## Licence

	Kajoom Framework for Bash - The Kajoom Framework for Bash.
	Copyright (C) 2021  Marc-Antoine Minville - KAJOOM

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

---

## Credits

[Marc-Antoine Minville - KAJOOM](https://www.kajoom.ca/) 

Many parts where inspired by my Bash guru, Mathieu! 
Thanks also to the stackoverflow community for having filled 75% of my searches as always ;)

---
