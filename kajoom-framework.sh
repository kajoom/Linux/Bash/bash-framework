#!/bin/bash
# Kajoom Framework for Bash Scripts
# Version: 0.2.2
# Authors : Marc-Antoine Minville - Kajoom.Ca
# 2021-01-12 (v0.2.2) - Improved framework and added tools.
# 2021-01-05 (v0.2.1) - Development version.
# 2021-01-02 (v0.2.0) - New bash version.
# Download as .sh file and run it. eg: 
# wget -O kajoom-framework.sh https://cloud.kajoom.net/s/kajoomframeworksh/download
# bash ./kajoom-framework.sh

########################################################################
# Kajoom Framework for Bash - The Kajoom Framework for Bash.
# Copyright (C) 2021  Marc-Antoine Minville - KAJOOM
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
########################################################################

# Do not load twice.
[ "yes" == KAJOOM_FRAMEWORK_LOADED ] && return 0;

# KEYWORD Kajoom Script Version. Be sure to keep the same as in header comments.
KAJOOM_FRAMEWORK_SCRIPT_VERSION='0.2.2'
KAJOOM_FRAMEWORK_SCRIPT_NAME='Kajoom Framework (Bash)'
KAJOOM_FRAMEWORK_SCRIPT_DESCRIPTION='Kajoom Framework for Bash Scripts'
KAJOOM_FRAMEWORK_SCRIPT_URL='https://cloud.kajoom.net/s/kajoomframeworksh'
KAJOOM_FRAMEWORK_SCRIPT_DOWNLOAD_URL='https://cloud.kajoom.net/s/kajoomframeworksh/download'
KAJOOM_FRAMEWORK_SCRIPT_FILENAME='kajoom-framework.sh'
KAJOOM_FRAMEWORK_SCRIPT_AUTHOR="Marc-Antoine Minville - KAJOOM" # Please do not change this value!
KAJOOM_FRAMEWORK_LOADED=yes
KFSV="${KAJOOM_FRAMEWORK_SCRIPT_VERSION}"


# Script and Command options presets.
VERBOSITY="${VERBOSITY:-}" # [""|v|vv|vvv]
COMMAND_OPTIONS="${COMMAND_OPTIONS:-}"
CPU_LOAD_MAX=${CPU_LOAD_MAX:-5.00}
STEPS="${STEPS:-}"
SCAN_MODE="${STEPS:-default}" # to remove or rename.
SCRIPT_ENV="${SCRIPT_ENV:-prod}" # [prod|stage|dev|alpha|beta|test]
SCRIPT_ENV_FILE=""

# Config preset.
CONFIG_FOLDERNAME=".kajoom"
CONFIG_FOLDER="${HOME}/${CONFIG_FOLDERNAME}"

# Date and Time.
LOG_DATE=$(date +%Y-%m-%d)
LOG_DATE_ALT=$(date +%Y%m%d)
LOG_TIME=$(date +%H:%M:%S)
LOG_TIME_ALT=$(date +%H%M%S)

# For menus.
_die() { echo "$*" >&2; exit 2; }  # complain to STDERR and exit with error
_needs_arg() { if [ -z "$OPTARG" ]; then _die "No arg for --$OPT option"; fi; }
_getSEED() { echo "${1}" | grep . | sed -e 's/[-_\. -+*/|&(){}^~;:.,?]//g' ; }

# Load Environment.
_loadENV() {
	
	SCRIPT_ENV_FILE="${SELF_LOC}/.env"
	if [ -f "${SCRIPT_ENV_FILE}" ] ; then
	  local NEW_ENV=$(cat "${SCRIPT_ENV_FILE}" | grep . | head -n 1)
	  [ -n "${NEW_ENV}" ] && SCRIPT_ENV="${NEW_ENV}"
	fi
	
	SCRIPT_ENV=${SCRIPT_ENV,,}
	SCRIPT_ENV=$(_getSEED "${SCRIPT_ENV}");
	local URL_SEED=$(_getSEED "${SCRIPT_FILENAME}");
	local URL_SEED_INI="${URL_SEED}"
	
	# Rewrite script variables values.
	if [ -n "${SCRIPT_ENV}" ] && [ "prod" != "${SCRIPT_ENV}" ] ; then
    
    URL_SEED="${URL_SEED}${SCRIPT_ENV}"
    CONFIG_FOLDERNAME="${CONFIG_FOLDERNAME}${SCRIPT_ENV}"
    
    #SCRIPT_VERSION="${SCRIPT_VERSION}-${SCRIPT_ENV}"
    SCRIPT_NAME="${SCRIPT_NAME} [${SCRIPT_ENV^^}]"
    SCRIPT_DESCRIPTION="${SCRIPT_DESCRIPTION} (${SCRIPT_ENV^^})"
    SCRIPT_URL="${SCRIPT_URL}${SCRIPT_ENV}"
    SCRIPT_ALIAS="${SCRIPT_ALIAS}-${SCRIPT_ENV}"
    #SCRIPT_FILENAME="${SCRIPT_FILENAME}"
    SCRIPT_DOWNLOAD_URL="https://cloud.kajoom.net/s/${URL_SEED}/download"
  fi
	
	return 0;
}
_loadENV;


# Temporary folders and files.
TEMP_DIR=$(mktemp -d -t ${SCRIPT_ALIAS}.XXXXXXXXXX)
TEMP_FILE=$(mktemp /tmp/${SCRIPT_ALIAS}.XXXXXXXX)

# Configs.
CONFIG_FOLDER="${HOME}/${CONFIG_FOLDERNAME}"
CONFIG_FILE="${CONFIG_FOLDER}/${SCRIPT_ALIAS}-config"

# Steps.
STEPS_FILE="${CONFIG_FOLDER}/${SCRIPT_ALIAS}-active"

# PID file.
PID_FILE="${SCRIPT_ALIAS}-pid.lock"
PID_FILE_PATH="${CONFIG_FOLDER}/${PID_FILE}"

# Resources
RESOURCES_LIST=()

# Logs.
LOG_FILENAME="${SCRIPT_ALIAS}.log"
LOG_FOLDER="${CONFIG_FOLDER}"
LOG_FILE="${LOG_FOLDER}/${LOG_FILENAME}"

# Admin logs.
# By befault, admin log file is the same than the regular log. Override it in the script if needed.
LOG_FILENAME_ADMIN="${LOG_FILENAME}"
LOG_FOLDER_ADMIN="${LOG_FOLDER}"
LOG_FILE_ADMIN="${LOG_FOLDER_ADMIN}/${LOG_FILENAME}"

# Reports.
REPORT_FILENAME="${SCRIPT_ALIAS}-report-${LOG_DATE}-${LOG_TIME_ALT}.log"
REPORTFILENAME="${REPORT_FILENAME}" # Backward compatibility, to remove later.
REPORTS_FOLDER="${CONFIG_FOLDER}/Reports"
REPORT_FILE="${REPORTS_FOLDER}/${REPORT_FILENAME}"


# Pad a string for display.
# 1:prefix, 2:string, 3:length, 4:start
_padSTRING() {
	
	local PREFIX=${1:-''}
	local STRING=${2:-''}
	local LENGTH=$(( ${3:-72} ))
	local START=$(( ${4:-1} ))
	
	LENGTH=$(( ${LENGTH} + ${START} ))
	
	[ -n "$PREFIX" ] && LENGTH=$(( ${LENGTH} - ${#PREFIX} ))
	[ ${START} -gt 0 ] && printf '%0.s ' $(seq 1 $START)
	[ -n "$PREFIX" ] && echo -n "${PREFIX}";
	
	echo "${STRING}" | sed -e :a -e "s/^.\{1,$LENGTH\}$/ &/;ta"
}


# Initialize progress bar.
_initPROGRESS() {
  
  # For progress bar.
  UNIT=1
  COUNTER=${COUNTER:-0}
  ALT_COUNTER=0
  local COUNT=$(( ${1:-0} ))
  FACTOR=$(( ${COUNT} / 34 ))
  MODULO=$(( ${COUNT} % 34 ))
  FACTOR_INV=$(( 34 / ${COUNT} ))
  MODULO_INV=$(( 34 % ${COUNT} ))
  [ ${FACTOR} -ne 0 ] && STEPSNUM=$(( ${COUNT} / ${FACTOR} )) || STEPSNUM=${COUNT}
  #[ ${FACTOR} -gt 0 ] && ALT_COUNTER=$(( ${MODULO:-1} / ${FACTOR} + $(( ${MODULO:-1} % ${FACTOR} )) ))
  [ ${FACTOR} -gt 1 ] && ALT_COUNTER=$(( -${MODULO:-1} ))
  [ ${FACTOR_INV} -gt 0 ] && UNIT=$(( ${FACTOR_INV} )) && COUNTER=${MODULO_INV:-1} || UNIT=${UNIT}

  if [ "-vvv" == "${VERBOSITY}" ] ; then
    echo;
    echo " Count: ${COUNT}"
    echo " Unit: ${UNIT}"
    echo " Factor: ${FACTOR}"
    echo " Factor Inv.: ${FACTOR_INV}"
    echo " Counter: ${COUNTER}"
    echo " Steps: ${STEPSNUM}"
  fi
}


# Spinner with progress bar.
function _showPROGRESS() {
	
	local PROGRESS_COUNTER=${1:-0}
	
	if [ ${FACTOR} -gt 1 ] ; then
		ALT_COUNTER=$(( ${ALT_COUNTER} + ${UNIT} ))
		if [ ${ALT_COUNTER} -ge ${FACTOR} ] ; then
			COUNTER=$(( ${PROGRESS_COUNTER} + ${UNIT} ))
			ALT_COUNTER=0
		fi
	else
		COUNTER=$(( ${PROGRESS_COUNTER} + ${UNIT} ))
	fi
  
  count=${COUNTER:-0}
  total=34
  pstr="[=======================================================================]"

  if [ $count -lt $total ] ; then
    #sleep 0.5 # this is work
    count=$(( $count + 1 ))
    pd=$(( $count * 73 / $total ))
    printf "\r%3d.%1d%% %.${pd}s" $(( $count * 100 / $total )) $(( ($count * 1000 / $total) % 10 )) ${pstr}
  fi

}

# Bash Spinner for Long Running Tasks.
# See : http://fitnr.com/showing-a-bash-spinner.html
#--
_showSPINNER()
{
    local pid=$1
    local delay=0.75
    local spinstr='|/-\'
    while [ "$(ps a | awk '{print $1}' | grep $pid)" ]; do
        local temp=${spinstr#?}
        printf " [%c]  " "$spinstr"
        local spinstr=$temp${spinstr%"$temp"}
        sleep $delay
        printf "\b\b\b\b\b\b"
    done
    printf "    \b\b\b\b"
}
#--


# Highlight a value with color.
# 1: value 2: text
_highlightVALUE() {
	
	if [ "" == "${1}" ] || [ "" == "${2}" ] ; then
	  echo "${2}"
	else
	  local CHECK=$(echo "${2}" | grep "${1}")
	  local NEW_MSG=$(colorText orange "${1}" -ne)
	  local NEW_TXT=$(echo "${2}" | sed "s/${1}/${NEW_MSG}/g")
	  [ "" != "${CHECK}" ] && NEW_TXT="${NEW_TXT}" || NEW_TXT="${2}"
	  echo "${NEW_TXT}"
	fi
	
	return 0;
}


# Check if file exists at url before downloading
# See : https://gist.github.com/hrwgc/7455343
# simple function to check http response code before downloading a remote file
# example usage:
# if `_validateURL $url >/dev/null`; then dosomething; else echo "does not exist"; fi
function _validateURL(){
  if [[ `wget -S --spider $1  2>&1 | grep 'HTTP/1.1 200 OK'` ]]; then echo "true"; fi
}


# Check if value is a MD5 sum.
_isMD5() {
	local TEST=$(echo "${1}" | grep -E '^[a-z0-9]{32}$')
	[ "" != "${1}" ] && [ "" != "${TEST}" ] && return 0 || return 1
}


# Check if a UNIX user exists on the system.
_userEXISTS() {
 if getent passwd "${1}" > /dev/null 2>&1; then
   echo "yes"
 else
   echo "no"
 fi
}


_checkLOAD() {
  # CPU Load
  #CPU_LOAD=$(top -bn1 | grep load)
  CPU_LOAD=$(uptime | cut -d: -f5 | cut -d ' ' -f2)
  CPU_LOAD="${CPU_LOAD/,/.}"
  #CPU_LOAD="${CPU_LOAD::-1}"
  CPU_LOAD=$(echo "$CPU_LOAD" | sed 's/.$//')
  #CPU_LOAD=$(echo $CPU_LOAD | awk '{printf "%.2f", $(NF-2)}')
  
  echo "$CPU_LOAD"
}


_getDURATION() {
	
  SCRIPT_END=$(date +%s)
  SCRIPT_DURATION=$(( $SCRIPT_END - $SCRIPT_START ))
  
  LOG_MSG="Script Execution Time:"
  
  if  [ "" != "${VERBOSITY}" ] ; then
    _padSTRING "${LOG_MSG}" "${SCRIPT_DURATION} sec" 52
  fi
  
  _writeLOG "${LOG_MSG} ${SCRIPT_DURATION} sec"
  
}


_scriptsINFOS() {
  
  local SHOW_VARS=()
  # Script.
  SHOW_VARS+=("title=Script" SCRIPT_NAME SCRIPT_VERSION SCRIPT_ENV SCRIPT_DESCRIPTION SCRIPT_URL SCRIPT_DOWNLOAD_URL SCRIPT_ALIAS SCRIPT_FILENAME SCRIPT_START)
  # Command and Options.
  [ -n "${VERBOSITY}" ] && [ "-v" != "${VERBOSITY}" ] && SHOW_VARS+=("title=Command" CURRENT_PID SELF_LOC SELF_PATH INIT_LOC VERBOSITY)
  # Others.
  [ "-vvv" == "${VERBOSITY}" ] && SHOW_VARS+=("title=Others" COMMAND_OPTIONS CPU_LOAD_MAX STEPS SCAN_MODE)
  # Date and Time.
  [ "-vvv" == "${VERBOSITY}" ] && SHOW_VARS+=("title=Date and Time" LOG_DATE LOG_DATE_ALT LOG_TIME LOG_TIME_ALT)
  # Configs.
  [ "-vv" == "${VERBOSITY}" ] || [ "-vvv" == "${VERBOSITY}" ] && SHOW_VARS+=("title=Configs" CONFIG_FOLDER CONFIG_FILE STEPS_FILE)
  # Logs.
  [ "-vv" == "${VERBOSITY}" ] || [ "-vvv" == "${VERBOSITY}" ] && SHOW_VARS+=("title=Logs" LOG_FILE LOG_FOLDER_ADMIN LOG_FILE_ADMIN)
  # Reports.
  [ "-vv" == "${VERBOSITY}" ] || [ "-vvv" == "${VERBOSITY}" ] && SHOW_VARS+=("title=Reports" REPORTS_FOLDER REPORT_FILENAME REPORT_FILE)
  # Temporary folders and files.
  [ "-vvv" == "${VERBOSITY}" ] && SHOW_VARS+=("title=Temporary" TEMP_DIR TEMP_FILE)
  # Framework.
  [ "-vvv" == "${VERBOSITY}" ] && SHOW_VARS+=("title=Framework" KAJOOM_FRAMEWORK_LOADED KAJOOM_FRAMEWORK_SCRIPT_VERSION KAJOOM_FRAMEWORK_SCRIPT_NAME KAJOOM_FRAMEWORK_SCRIPT_DESCRIPTION KAJOOM_FRAMEWORK_SCRIPT_URL KAJOOM_FRAMEWORK_SCRIPT_DOWNLOAD_URL KAJOOM_FRAMEWORK_SCRIPT_FILENAME)
  
  for ((i = 0; i < ${#SHOW_VARS[@]}; i++)); do
  #for var in ${SHOW_VARS[@]}; do
    local var="${SHOW_VARS[$i]}"
    if [[ "$var" == title=* ]] ; then
      local TITLE=$(echo "$var" | sed 's/^title=//g')
      TITLE=$(colorText purple "[$TITLE]")
      [ -n "${VERBOSITY}" ] && [ "-v" != "${VERBOSITY}" ] && _padSTRING "$TITLE" "" 69
    else
      _padSTRING "$var: " "${!var}" 52
    fi
  done
}


_processHEADER() {
	
	local STRING_ARRAY=(${SCRIPT_NAME})
	local TITLE1="${STRING_ARRAY[0]^^}"
	local TITLE2=""
	
	[ -n "${STRING_ARRAY[1]}" ] && TITLE1="${TITLE1} ${STRING_ARRAY[1]^^}"
	[ -n "${STRING_ARRAY[2]}" ] && TITLE2="${TITLE2} ${STRING_ARRAY[2]^^}"
	[ -n "${STRING_ARRAY[3]}" ] && TITLE2="${TITLE2} ${STRING_ARRAY[3]^^}" 
	[ -n "${STRING_ARRAY[4]}" ] && TITLE2="${TITLE2} ${STRING_ARRAY[4]^^}"
	[ -n "${STRING_ARRAY[5]}" ] && TITLE2="${TITLE2} ${STRING_ARRAY[5]^^}"
	
  #TITLE1=$(_padSTRING "" ":: ${TITLE1}" 31 0)
  #TITLE2=$(_padSTRING "" "${TITLE2} ::" 31 0)
	
	printf "${1}" | sed "s/TITLE1/${TITLE1}/g" | sed "s/TITLE2/${TITLE2}/g"
}


_processSUBTITLE() {
	
	local STRING_ARRAY=(${SCRIPT_DESCRIPTION})
	local SUBTITLE="${SCRIPT_DESCRIPTION}"
	local WORD="${STRING_ARRAY[0]}"
	
	#[ -n "${STRING_ARRAY[1]}" ] && WORD="${STRING_ARRAY[1]}"
	#[ -n "${STRING_ARRAY[2]}" ] && WORD="${STRING_ARRAY[2]}"
	[ -n "${STRING_ARRAY[3]}" ] && STRING_ARRAY[3]=$(colorText green "${STRING_ARRAY[3]}" -ne)
	#[ -n "${STRING_ARRAY[4]}" ] && WORD="${STRING_ARRAY[4]}"
	
	#SUBTITLE=_highlightVALUE "${WORD}" "${SUBTITLE}"
	#local NEW_MSG=$(colorText green "${WORD}" -ne)
	#local NEW_TXT=$(printf "${SCRIPT_DESCRIPTION}" | sed "/${WORD}/${NEW_MSG}/")
	
	printf "${STRING_ARRAY[*]}"
}


#
#  name: _showHEADER
#  @param none
#  @return Outputs a header with description.
#
_showHEADER() {
	
  # SCRIPT_NAME
  # SCRIPT_DESCRIPTION
  # SCRIPT_VERSION
  # SCRIPT_FILENAME
  TITLE=$(_processHEADER "TITLE1TITLE2")
  SUBTITLE=$(_processSUBTITLE)
  
  _padSTRING "++++" " :: ${TITLE} (v${KFSV}) ::        ++++" 68
  _padSTRING "|+        " " ${SUBTITLE}                +|" 85
  echo -e " ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ \n"
}


_showFOOTER() {
	
  if  [ "" != "${VERBOSITY}" ] ; then
    colorText white " ====================================================== "
    _scriptsINFOS
  fi
  
	_getDURATION;
	colorText white " ====================================================== "
	echo;
}


# Function colorText()
# $1 : color name
# $2 : text string
# $3 : -n for no new line
_colorText() {
	
	# Available Colors
	green=119
	gray=252
	mint=192
	red=196
	white=256
	# New colors.
	orange=172
	blue=032
	yellow=184
	pink=212
	purple=141
	
  # Default Color
  myColor=256 # white
  
  if [ -n "${1}" ] 
  then
  	#echo $(($1))
  	myColor="$(($1))"
  fi
  
  local ARGS='-e'
  if [ -n "${3}" ] ; then
    ARGS='-ne'
  fi
  
  echo ${ARGS} "\033[1;38;5;${myColor}m${2}\033[0m"
}


# Alias of _colorText function.
colorText() {
	_colorText "$@";
}


_printSTEP() {
	
  # Wrap-up.
  touch "${STEPS_FILE}"
  DONE_STEP=$(_doneSTEP;)
  echo -e "\033[1;38;5;192m${1}\033[0m${DONE_STEP}"
  #colorText white "Step : ${STEP}" ; sleep 0.05 ;
}


_doneSTEP() {
	
	if [[ ! -z $(grep -Fx "$STEP" "$STEPS_FILE") ]] ; then
		echo " \033[1;38;5;160m[DONE]\033[0m"
	fi
}


_addSTEP() {
	
  # Log latest step.
  touch "${STEPS_FILE}"
  echo "$STEP" >> "${STEPS_FILE}"
  _writeLOG "-----------------------------------------------------------------------"
  _writeLOG "Completed step: $STEP."
}


_getRESOURCES() {
	echo "${RESOURCES_LIST[@]}" | sed 's/ /\n/g'
}

_getRESOURCENAME() {
	echo "${1}" | head -n 1 | cut -d= -f1
}

_getRESOURCEPATH() {
	echo "${1}" | head -n 1 | cut -d= -f2
}

_getRESOURCELINE() {
	_getRESOURCES | grep "^$1=" | head -n 1
}

# _getRESOURCE log
_getRESOURCE() {
	
	local RESOURCE_LINE=$(_getRESOURCELINE "$1")
	local RESOURCE=$(_getRESOURCEPATH "${RESOURCE_LINE}")
	
	if [ -n "$1" ] && [ -n "$RESOURCE" ] ; then
	  echo "${RESOURCE}"
	  return 0;
	else
	  colorText red " Unable to find resource '$1'."
	  return 1;
	fi
}


_addRESOURCE() {
	
	if [ -n "$1" ] && [ -n "$2" ] ; then
	  if _getRESOURCE "$1" >/dev/null ; then 
	    colorText red " Resource '${1}' already exists!"
	    return 1 ; 
	  fi
	  local RESOURCE="${1}=${2}"
	  RESOURCES_LIST+=("${RESOURCE}" )
	  return 0;
	else
	  colorText red " Unable to register resource '$1' : $2"
	  return 1;
	fi
}

createRESOURCE() {
	
	local ERRORS=0
	local NAME="$1"
	local RESOURCE=$(_getRESOURCE "${NAME}")
	local RESOURCE_FOLDER=$(dirname "${RESOURCE}")
	
	[ -z "${NAME}" ] || [ -z "${RESOURCE}" ] && return 1;
	  
  mkdir -p "${RESOURCE_FOLDER}"
  if [ -w "${RESOURCE_FOLDER}/" ] && touch "${RESOURCE}" ; then
   local LOG_MSG="Created resource '${NAME}' : ${RESOURCE}"
   _writeLOG "${LOG_MSG}."
  else
	  local LOG_MSG="Unable to create resource '${NAME}' : ${RESOURCE}"
	  colorText red "${LOG_MSG}!"
	  _writeLOG "${LOG_MSG}."
	  ERRORS=$(( ${ERRORS} + 1 ))
  fi
	
	return ${ERRORS} ;
}

createRESOURCES() {
	
	local ERRORS=0
	
	for res in "${RESOURCES_LIST[@]}" ; do
	  local NAME=$(_getRESOURCENAME "${res}")
	  #local RESOURCE=$(_getRESOURCEPATH "${res}")
	  #local RESOURCE_FOLDER=$(dirname "${RESOURCE}")
    if ! createRESOURCE "${NAME}" ; then
      ERRORS=$(( ${ERRORS} + 1 ))
    fi
	done;
	
	return ${ERRORS} ;
}


_logINIT() {
	
	# Create Config folder if not exists.
  mkdir -p "${CONFIG_FOLDER}"
	touch "${LOG_FILE_ADMIN}"
}


_writeLOG() {
	_logINIT;
	
  local DATE_NOW=$(date +%Y-%m-%d)
  local TIME_NOW=$(date +%H:%M:%S)
  local DEFAULT_MESSAGE="Loggued action default message."
  local MESSAGE="${1:-Message}"
  local TYPE="${2:-'log'}"
  local LOG_MSG="${DATE_NOW} ${TIME_NOW} : ${MESSAGE}"
  
	if [ -f "${LOG_FILE}" ] || [ -f "${LOG_FILE_ADMIN}" ] || [ -f "${REPORT_FILE}" ] ; then
    # General log.
    [ "all" == "${TYPE}" ] || [ "log" == "${TYPE}" ] || [ -z "${TYPE}" ] && [ -f "${LOG_FILE}" ] && echo "${LOG_MSG}" >> "${LOG_FILE}"
    # Admin log.
    [ "all" == "${TYPE}" ] || [ "admin" == "${TYPE}" ] && [ -f "${LOG_FILE_ADMIN}" ] && echo "${LOG_MSG}" >> "${LOG_FILE_ADMIN}"
    # Report log.
    [ "all" == "${TYPE}" ] || [ "report" == "${TYPE}" ] && [ -f "${REPORT_FILE}" ] && echo "${LOG_MSG}" >> "${REPORT_FILE}"
    return 0;
  else
    colorText red "No log file found!"
    return 1;
  fi
}


_addCHECK() {
	
	CHECK="${1:-$CHECK}"
	DETAILS="${2:-}"
	local COLOR=mint
	local PUNCT="..."
	local LOG_MSG="Check for ${CHECK}"
	
	if [ -z "${CHECK}" ] ; then
    LOG_MSG="Empty check variable"
    COLOR=red
    PUNCT="!"
  fi
  
  colorText ${COLOR} " ${LOG_MSG}${PUNCT}" ; sleep 0.05 ;
  [ -n "${DETAILS}" ] && [ -n "${VERBOSITY}" ] && colorText gray "\n Test explained: ${DETAILS}" ;
  echo;
  _writeLOG "${LOG_MSG}."
}


_addSUCCESS() {
	
	if [ ${SUCCESS} -ge 0 ] ; then
    SUCCESS=$(( ${SUCCESS} + 1 ))
    LOG_MSG="Sucessfully completed test '${CHECK}'"
    echo " - ${LOG_MSG}." ; sleep 0.05 ;
    _writeLOG "${LOG_MSG}."
    echo;
    return 0;
  else
    LOG_MSG="Corrupt success variable"
    colorText red " ${LOG_MSG}!" ; sleep 0.05 ;
    _writeLOG "${LOG_MSG}."
    echo;
    return 1;
  fi
}


_addWARNING() {
	
	if [ ${WARNINGS} -ge 0 ] ; then
    WARNINGS=$(( ${WARNINGS} + 1 ))
    LOG_MSG="Warning raised for test '${CHECK}'"
    colorText orange " - ${LOG_MSG}!" ; sleep 0.05 ;
    _writeLOG "${LOG_MSG}."
    echo;
    return 0;
  else
    LOG_MSG="Corrupt warnings variable"
    colorText red " ${LOG_MSG}!" ; sleep 0.05 ;
    _writeLOG "${LOG_MSG}."
    echo;
    return 1;
  fi
}


_addERROR() {
	
	if [ ${ERRORS} -ge 0 ] ; then
    ERRORS=$(( ${ERRORS} + 1 ))
    LOG_MSG="Check failed for test '${CHECK}'"
    colorText red " - ${LOG_MSG}!" ; sleep 0.05 ;
    _writeLOG "${LOG_MSG}."
    echo;
    return 0;
  else
    LOG_MSG="Corrupt errors variable"
    colorText red " ${LOG_MSG}!" ; sleep 0.05 ;
    _writeLOG "${LOG_MSG}."
    echo;
    return 1;
  fi
}


_askPROCEED() {
	
	DEFAULT=n
	if [ "-y" == "$1" ] ; then
	  DEFAULT=y
	fi
	
  # Proceed?
  unset PROCEED
  if [ "$SCAN_MODE" == "auto" ] || [ "$SCAN_MODE" == "batch" ] ; then
    PROCEED="y"
    DEFAULT="y"
  else
  
    if [ "$DEFAULT" == "y" ] ; then
	    colorText white "\n Do you wish to proceed ? (Y/n)"
    else
	    colorText white "\n Do you wish to proceed ? (y/N)"
    fi
	  read -e PROCEED
	fi
	
	PROCEED="${PROCEED:-${DEFAULT}}"
}


_cleanTEMP() {
	# Clean Temp files.
	if [ -d "${TEMP_DIR}" ] ; then
    rm -rf "${TEMP_DIR}"
  fi
	if [ -f "${TEMP_FILE}" ] ; then
    rm "${TEMP_FILE}"
  fi
	if [ -d "${DOWNLOAD_TEMP_DIR}" ] ; then
    rm r "${DOWNLOAD_TEMP_DIR}"
  fi
}


_displayLIST() {
	
	if [ -z "$1" ] && [ -z "$RESULT" ] ; then 
	  return 1 ; 
	fi
	
	local LIST="${1:-$RESULT}"
	local COUNT=$(echo "${LIST}" | wc -l)
	
  if [ ${COUNT} -gt 20 ] ; then
    
    colorText mint " First 20 results (from ${COUNT} total)";
    echo;
    echo "${LIST}" | tail -n 20;
    echo;
    
    echo " Display all entries?"
    _askPROCEED;
    if [ "$PROCEED" == y ] ; then
      echo "${LIST}" | less;
    fi
  
  else
    colorText mint " Results (${COUNT})";
    echo;
    echo "${LIST}";
    echo;
  fi
}


_getITEM() {
	
	echo ""
	colorText green " ${SCRIPT_NAME} :: Display ${ITEM_NAME}"
	echo ""
	
	if [ "" == "${1}" ] ; then
		  colorText red " Argument missing!"
		  _cleanTEMP;
		  echo;
		  exit 1;
	fi
	
	ITEM_FILE="${ITEM_FOLDER}/${1}"
	if [ ! -f "${ITEM_FILE}" ] ; then
		  colorText red " ${ITEM_NAME} missing!"
		  _cleanTEMP;
		  echo;
		  exit 1;
	fi
	
	RESULT=$(cat ${ITEM_FILE})
	
	_displayLIST "${RESULT}"
}


_getITEMS() {
	
	echo ""
	colorText green " ${SCRIPT_NAME} :: Get ${ITEM_NAME}"
	echo;
	
	if [ ${#ITEM_MSG[@]} -gt 0 ] ; then
	  printf '%s\n' "${ITEM_MSG[@]}";
	  echo;
	fi
	RESULT=''
  PARAMS="${PARAMS:-}"
	if ls -1 ${FILES_ARG} 1> /dev/null 2>&1; then
	 RESULT=$(ls -1 ${FILES_ARG} | xargs -n 1 basename)
	fi
	if [ "" != "${PARAMS}" ] ; then
	  RESULT=$(echo "${RESULT}" | ${PARAMS})
	fi
  
  _displayLIST "${RESULT}"
}


_selfUPDATE() {
	
  # Verbosity.
  if [ "$1" == "-v" ] || [ "$1" == "-vv" ] || [ "$1" == "-vvv" ] ; then
    VERBOSITY="${1}"
  fi
	
	SELF_PATH=$(realpath $0)
	SCRIPT_VERSION_TXT=$(_colorText white "${SCRIPT_VERSION}")
	
	echo;
	colorText green " Update ${SCRIPT_FILENAME} Script" ; sleep 0.2 ;
	echo;
	
	SCRIPT_NAME_TXT=$(_colorText white "${SCRIPT_NAME}")
	echo " - Script Name : ${SCRIPT_NAME_TXT}" ; sleep 0.05 ;
	#echo " - Actual Version : ${SCRIPT_VERSION_TXT}" ; sleep 0.05 ;
	
	if [ -f "${SELF_PATH}" ] ; then
	  echo " - Script Path : ${SELF_PATH}" ; sleep 0.05 ;
	else
	  colorText red " - Error: Script path is not a file : ${SELF_PATH}" ; sleep 0.05 ;
	  exit 1;
	fi
	
	echo " - Download URL : ${SCRIPT_DOWNLOAD_URL}" ; sleep 0.05 ;
	
	#_askPROCEED -y;
	if [ "" != "$SCRIPT_FILENAME" ] && [ "" != "$SCRIPT_DOWNLOAD_URL" ] && [ "" != "$SELF_PATH" ] ; then
	  
	  echo;
	  
    # Download.
    MY_TEMP_DIR=$(mktemp -d -t "${SCRIPT_ALIAS}.XXXXXXXXXX")
    MY_TEMP_DOWNLOAD_FILE="${MY_TEMP_DIR}/${SCRIPT_FILENAME}"
    if `_validateURL ${SCRIPT_DOWNLOAD_URL} >/dev/null`; then
      #wget -O "${SELF_PATH}" "${SCRIPT_DOWNLOAD_URL}"
      LOCAL_DOWNLOAD=$(wget -O "${MY_TEMP_DOWNLOAD_FILE}" "${SCRIPT_DOWNLOAD_URL}" >/dev/null 2>&1)
      if [ "" != "${VERBOSITY}" ] ; then
        echo "${LOCAL_DOWNLOAD}" ; sleep 0.05 ;
      fi
    else
      colorText red " Download URL is not reachable : ${SCRIPT_DOWNLOAD_URL}" ; sleep 0.05 ;
      _cleanTEMP;
      echo;
      exit 1;
    fi
	  
	  # Verify file.
	  # TODO: Verify MD5.
	  # 
	  #wget -O "${SELF_PATH}" "${SCRIPT_DOWNLOAD_URL}"
	  # Check if Bash script.
	  CHECK_BASH=$(head -n 1 "${MY_TEMP_DOWNLOAD_FILE}" | grep '#!/bin/bash' )
	  if [ "" == "${CHECK_BASH}" ] ; then
      colorText red " Downloaded file is not Bash!" ; sleep 0.05 ;
      echo "  ${CHECK_BASH}" ; sleep 0.05 ;
      _cleanTEMP;
      echo;
      exit 1;
	  fi
	  # Check if contain mandatory script file name variable.
	  CHECK_FILENAME=$(cat "${MY_TEMP_DOWNLOAD_FILE}" | grep "SCRIPT_FILENAME='${SCRIPT_FILENAME}'" | head -n 1)
	  if [ "" == "${CHECK_FILENAME}" ] ; then
      colorText red " Does not contain mandatory 'SCRIPT_FILENAME' variable!" ; sleep 0.05 ;
      echo "  ${CHECK_FILENAME}"
      _cleanTEMP;
      echo;
      exit 1;
	  fi
	  # Check if contain mandatory script version variable.
	  CHECK_VERSION=$(cat "${MY_TEMP_DOWNLOAD_FILE}" | grep "SCRIPT_VERSION='" | head -n 1)
	  if [ "" == "${CHECK_VERSION}" ] ; then
      colorText red " Does not contain mandatory 'SCRIPT_VERSION' variable!" ; sleep 0.05 ;
      _cleanTEMP;
      echo;
      exit 1;
	  fi
	  
	  # Check Versions.
	  SCRIPT_VERSION_TXT=$(_colorText mint "${SCRIPT_VERSION}")
	  NEW_VERSION=$(echo "${CHECK_VERSION}" | sed 's/SCRIPT_VERSION=//g' | sed s/\'//g)
	  HEADER_VERSION=$(head "${MY_TEMP_DOWNLOAD_FILE}" | grep "^# Version: " | sed 's/# Version: //g' | sed 's/ //g')
	  HEADER_WARNING=""
	  
	  [ "${NEW_VERSION}" == "${SCRIPT_VERSION}" ] && MYCOLOR=orange || MYCOLOR=green
	  
	  [ "${NEW_VERSION}" != "${HEADER_VERSION}" ] && MYCOLOR=orange && HEADER_WARNING="Version in header differs from the one registered, this may not be a problem but worth a check"
	  
	  NEW_VERSION_TXT=$(_colorText "${MYCOLOR}" "${NEW_VERSION}")
	  
	  echo " Actual version:   ${SCRIPT_VERSION_TXT}" ; sleep 0.05 ;
	  echo " New version:      ${NEW_VERSION_TXT}" ; sleep 0.05 ;
	  
	  MYANSWER="-y"
	  if [ "${NEW_VERSION}" == "${SCRIPT_VERSION}" ] ; then
	    LOG_MSG="Versions are the same"
	    echo;
	    _colorText orange " ${LOG_MSG}!" ; sleep 0.05 ;
	    _writeLOG "${LOG_MSG}."
	    MYANSWER="-n"
	  fi
	  
	  if [ "${NEW_VERSION}" != "${HEADER_VERSION}" ] ; then
	    echo;
	    _colorText orange " ${HEADER_WARNING}!" ; sleep 0.05 ;
	    _writeLOG "${HEADER_WARNING}."
	  fi
	  
	  _askPROCEED "${MYANSWER}";
	  if [ "$PROCEED" == y ] ; then
	  
	    colorText mint " Updating script..." ; sleep 0.05 ;
	    echo;
	  
	    # Update the script, finally!
	    UPDATE_RESULT=$(\cp -av "${MY_TEMP_DOWNLOAD_FILE}" "${SELF_PATH}")
      if [ "" != "${VERBOSITY}" ] ; then
        echo "${UPDATE_RESULT}" ; sleep 0.05 ;
      fi
	    
      LOG_MSG="Script updated to version ${NEW_VERSION}"
      colorText green " ${LOG_MSG}!" ; sleep 0.05 ;
      _writeLOG "${LOG_MSG}."
    
    else
      LOG_MSG="Aborted update"
      colorText gray " ${LOG_MSG}!" ; sleep 0.05 ;
      _writeLOG "${LOG_MSG}."
	  fi
    
    # Clean.
    _cleanTEMP;
    unset LOCAL_DOWNLOAD MY_TEMP_DIR MY_TEMP_DOWNLOAD_FILE
    
  fi
  
  echo;
}


# Wizard. Should be overrided in the script, unless this is sufficient!
_getUSERINFOS() {
	
	_askPROCEED;
}


_getSCRIPTAUTHOR() {
	
	local AUTHOR="${KAJOOM_FRAMEWORK_SCRIPT_AUTHOR}"
	# Check if a script is loaded and script author has been defined.
	[ -n "${SCRIPT_VERSION}" ] && [ -n "${SCRIPT_AUTHOR}" ] && AUTHOR="${SCRIPT_AUTHOR}"
	
	echo "${AUTHOR}"
	
	return 0;
}


_getSCRIPTNAME() {
	
	local NAME="${KAJOOM_FRAMEWORK_SCRIPT_NAME}"
	# Check if a script is loaded and script name has been defined.
	[ -n "${SCRIPT_VERSION}" ] && [ -n "${SCRIPT_NAME}" ] && NAME="${SCRIPT_NAME}"
	
	echo "${NAME}"
	
	return 0;
}


_getSCRIPTDESCRIPTION() {
	
	local DESCRIPTION="${KAJOOM_FRAMEWORK_SCRIPT_DESCRIPTION}"
	# Check if a script is loaded and script description has been defined.
	[ -n "${SCRIPT_VERSION}" ] && [ -n "${SCRIPT_DESCRIPTION}" ] && DESCRIPTION="${SCRIPT_DESCRIPTION}"
	
	echo "${DESCRIPTION}"
	
	return 0;
}


# Display License Information.
_displayLICENSEWARNING() {
	
	local AUTHOR=$(_getSCRIPTAUTHOR)
  
  echo " ${SCRIPT_NAME}  Copyright (C) 2021  ${AUTHOR}"
  if [ "${AUTHOR}" != "${KAJOOM_FRAMEWORK_SCRIPT_AUTHOR}" ] ; then 
  echo " Based on the ${KAJOOM_FRAMEWORK_SCRIPT_NAME}. "
  fi
  echo " This program comes with ABSOLUTELY NO WARRANTY; for details type '--license'."
  echo " This is free software, and you are welcome to redistribute it"
  echo " under certain conditions; type '--license' for details."
	echo ""
}


# Display License Information.
_displayLICENSE() {
	
	local AUTHOR=$(_getSCRIPTAUTHOR)
	local NAME=$(_getSCRIPTNAME)
	local DESCRIPTION=$(_getSCRIPTDESCRIPTION)
	
  echo " ######################################################################## "
  _padSTRING "# " " #" 70
  _padSTRING "# ${NAME} - ${DESCRIPTION}" " #" 70
  _padSTRING "# Copyright (C) 2021  ${AUTHOR}" " #" 70
  if [ "${AUTHOR}" != "${KAJOOM_FRAMEWORK_SCRIPT_AUTHOR}" ] || [ "${NAME}" != "${KAJOOM_FRAMEWORK_SCRIPT_NAME}" ] || [ "${DESCRIPTION}" != "${KAJOOM_FRAMEWORK_SCRIPT_DESCRIPTION}" ] ; then 
  _padSTRING "# " " #" 70
  _padSTRING "# Based on the ${KAJOOM_FRAMEWORK_SCRIPT_NAME}." " #" 70
  _padSTRING "# " " #" 70
  echo " # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ #"
  _padSTRING "# ${KAJOOM_FRAMEWORK_SCRIPT_NAME} - ${KAJOOM_FRAMEWORK_SCRIPT_DESCRIPTION}" " #" 70
  _padSTRING "# Copyright (C) 2021  ${KAJOOM_FRAMEWORK_SCRIPT_AUTHOR}" " #" 70
  echo " # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ #"
  fi
  _padSTRING "# " " #" 70
  _padSTRING "# This program is free software: you can redistribute it and/or" " #" 70
  _padSTRING "# modify it under the terms of the GNU General Public License as" " #" 70
  _padSTRING "# published by the Free Software Foundation, either version 3 of" " #" 70
  _padSTRING "# the License, or (at your option) any later version." " #" 70
  _padSTRING "# " " #" 70
  _padSTRING "# This program is distributed in the hope that it will be useful," " #" 70
  _padSTRING "# but WITHOUT ANY WARRANTY; without even the implied warranty of" " #" 70
  _padSTRING "# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the" " #" 70
  _padSTRING "# GNU General Public License for more details." " #" 70
  _padSTRING "# " " #" 70
  _padSTRING "# You should have received a copy of the GNU General Public License" " #" 70
  _padSTRING "# along with this program. If not, see https://www.gnu.org/licenses/." " #" 70
  _padSTRING "# " " #" 70
  echo " ######################################################################## "
	echo ""
}


# Display Version Information.
_displayVERSION() {
  
  echo ""
	echo " ${SCRIPT_NAME} (v${SCRIPT_VERSION})"
	echo ""
}


# Display Help Menu.
_displayHELP() {
  
  # Display Help
	echo ""
	colorText green " ${SCRIPT_NAME} (v${SCRIPT_VERSION})"
	echo ""
	colorText white " ${SCRIPT_DESCRIPTION}"
	echo ""
	echo " Usage: `basename $0` [-h|--help]"
	echo ""
	echo " Getting Started:"
	echo ""
	echo " - Download as .sh file and run it. eg:"
	echo ""
	echo "   wget -O ${SCRIPT_FILENAME} ${SCRIPT_DOWNLOAD_URL}"
	echo "   bash ${SCRIPT_FILENAME}"
	echo ""
	echo " Options:"
	echo ""
	echo "       --self-update          Update this script and exit."
	echo "       --auto                 No wizard and do not prompt."
	echo "   -v, -vv, -vvv              Adjust verbosity."
	echo "   -h, --help                 Display this help and exit."
	echo "       --license              Output license information and exit."
	echo "       --version              Output version information and exit."
	echo ""
	echo " The script contains a wizard to help define settings. "
	echo ""
	echo " The script will write its logs into the file located here: "
	echo " ${LOG_FILE_ADMIN}"
	echo ""
	_displayLICENSEWARNING;
}


# Menu Controller.
_menuCONTROLLER() {
	
#args=("$@")
#"${args[0]}"
 
# Verbosity.
if [ "$2" == "-v" ] || [ "$2" == "-vv" ] || [ "$2" == "-vvv" ] ; then
  VERBOSITY="${2}"
fi

# Help Menu.
if [ "$1" == "-h" ] || [ "$1" == "--help" ] ; then
  _displayHELP;
  _cleanTEMP;
  exit 0;
fi

# License Information.
if [ "$1" == "--license" ] ; then
  _showHEADER;
  _displayLICENSE;
  _cleanTEMP;
  exit 0;
fi

# Version Information.
if [ "$1" == "--version" ] ; then
  _displayVERSION;
  _cleanTEMP;
  exit 0;
fi

# Self Update Script.
if [ "$1" == "--self-update" ] ; then
  _selfUPDATE;
  _getDURATION;
  _cleanTEMP;
  exit 0;
fi

# Verbosity.
if [ "$1" == "-v" ] || [ "$1" == "-vv" ] || [ "$1" == "-vvv" ] ; then
  VERBOSITY="${1}"
fi

} # Menu Controller end. #

