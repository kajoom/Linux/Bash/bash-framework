#!/bin/bash
# Overrides for the Kajoom Framework.
# Version: 0.2.2
# Authors : Marc-Antoine Minville - Kajoom.Ca
# 2021-01-11 (v0.2.2) - New bash version.
# Download as .sh file and run it. eg: 
# wget -O kajoom-framework-overrides.sh https://cloud.kajoom.net/s/kajoomframeworkoverridessh/download
# bash ./kajoom-framework-overrides.sh

########################################################################
# Kajoom Framework for Bash - The Kajoom Framework for Bash.
# Copyright (C) 2021  Marc-Antoine Minville - KAJOOM
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
########################################################################

# Do not load twice.
[ "yes" == OVERRIDES_FRAMEWORK_LOADED ] && return 0;

# KEYWORD Kajoom Script Version. Be sure to keep the same as in header comments.
OVERRIDES_FRAMEWORK_SCRIPT_VERSION='0.2.2'
OVERRIDES_FRAMEWORK_SCRIPT_NAME='Kajoom Framework Overrides'
OVERRIDES_FRAMEWORK_SCRIPT_DESCRIPTION='KAJOOM Custom Overrides for Kajoom Framework Bash Scripts'
OVERRIDES_FRAMEWORK_SCRIPT_URL='https://cloud.kajoom.net/s/kajoomframeworkoverridessh'
OVERRIDES_FRAMEWORK_SCRIPT_DOWNLOAD_URL='https://cloud.kajoom.net/s/kajoomframeworkoverridessh/download'
OVERRIDES_FRAMEWORK_SCRIPT_FILENAME='kajoom-framework-overrides.sh'
OVERRIDES_FRAMEWORK_SCRIPT_AUTHOR="KAJOOM"
OVERRIDES_FRAMEWORK_LOADED=yes


# _processHEADER (override for KAJOOM)
_processHEADER() {
	
	local STRING_ARRAY=(${SCRIPT_NAME})
	local TITLE1="${STRING_ARRAY[0]^^}"
	local TITLE2=""
	
	[ -n "${STRING_ARRAY[1]}" ] && TITLE1="${TITLE1} ${STRING_ARRAY[1]^^}"
	[ -n "${STRING_ARRAY[2]}" ] && TITLE2="${TITLE2} ${STRING_ARRAY[2]^^}"
	[ -n "${STRING_ARRAY[3]}" ] && TITLE2="${TITLE2} ${STRING_ARRAY[3]^^}" 
	[ -n "${STRING_ARRAY[4]}" ] && TITLE2="${TITLE2} ${STRING_ARRAY[4]^^}"
	[ -n "${STRING_ARRAY[5]}" ] && TITLE2="${TITLE2} ${STRING_ARRAY[5]^^}"
	
  TITLE1=$(_padSTRING "" ":: ${TITLE1}" 31 0)
  TITLE2=$(_padSTRING "" "${TITLE2} ::" 31 0)
	
	printf "${1}" | sed "s/TITLE1/${TITLE1}/g" | sed "s/TITLE2/${TITLE2}/g"
}


# _processSUBTITLE (override for KAJOOM)
_processSUBTITLE() {
	
	local STRING_ARRAY=(${SCRIPT_DESCRIPTION})
	local SUBTITLE="${SCRIPT_DESCRIPTION}"
	local WORD="${STRING_ARRAY[0]}"
	
	#[ -n "${STRING_ARRAY[1]}" ] && WORD="${STRING_ARRAY[1]}"
	#[ -n "${STRING_ARRAY[2]}" ] && WORD="${STRING_ARRAY[2]}"
	[ -n "${STRING_ARRAY[3]}" ] && STRING_ARRAY[3]=$(colorText green "${STRING_ARRAY[3]}" -ne)
	#[ -n "${STRING_ARRAY[4]}" ] && WORD="${STRING_ARRAY[4]}"
	
	#SUBTITLE=_highlightVALUE "${WORD}" "${SUBTITLE}"
	#local NEW_MSG=$(colorText green "${WORD}" -ne)
	#local NEW_TXT=$(printf "${SCRIPT_DESCRIPTION}" | sed "/${WORD}/${NEW_MSG}/")
	
	printf "${STRING_ARRAY[*]}"
}


#
#  name: _showHEADER (override for KAJOOM)
#  @param none
#  @return outputs a brand logo for kajoom, for fun.
#
_showHEADER() {

# SCRIPT_NAME
# SCRIPT_DESCRIPTION
# SCRIPT_VERSION
# SCRIPT_FILENAME

SUBTITLE=$(_processSUBTITLE)

tput bold
cat <<- "__EOF__"

 888                d8b
 888                Y8P
 888
 888  888  8888b.  8888  .d88b.   .d88b.  88888b.d88b.
 888 .88P     "88b "888 d88""88b d88""88b 888 "888 "88b
 888888K  .d888888  888 888  888 888  888 888  888  888
 888 "88b 888  888  888 Y88..88P Y88..88P 888  888  888
 888  888 "Y888888  888  "Y88P"   "Y88P"  888  888  888
                   888
__EOF__

_processHEADER "   KAJOOM TOOLS   d88P TITLE1\n"
_processHEADER "                888P   TITLE2\n"
echo;
tput sgr0;

sleep 0.2 ;

colorText white " ====================================================== "
#echo -e " KAJOOM Framework \033[1;38;5;119mScript\033[0m ;-)                   | "
_padSTRING "|   " "${SUBTITLE}           | " 70
colorText white " ====================================================== "
echo -e "                  Version : $KFSV                 \n" ;

sleep 0.05 ;
}
